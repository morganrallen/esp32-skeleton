# esp32-skeleton

Does what it says on the tin.

# Prerequisites
Have [esp-idf](https://github.com/espressif/esp-idf/) cloned and `IDF_PATH` pointing to it's location.

# Quick Start
```
mkdir project
cd project
git clone https://github.com/espressif/esp-idf/
export IDF_PATH=`pwd`/esp-idf

git clone https://gitlab.com/morganrallen/esp32-skeleton.git
cd esp32-skeleton
make defconfig # may need to run make menuconfig for your specific device
make flash
make monitor
```
